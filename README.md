# Bicara Plugin - Recording

Bicara Recording Plugin actively checks whether recording needs to be initiated when a Bicara meeting starts and receives notification when the Vonage recording file is pushed to the S3 bucket.

Vonage Video API archiving lets us record, save and retrieve session recordings with easy integration with cloud storage services. On the Vonage Project page, scroll down to the **Archiving** section. Click **Edit** to configure the Cloud Storage connection. Since Bicara uses quite a few other AWS services to support video transcoding, transcription generation and event notifications, Amazon S3 Bucket is used for the Vonage archiving storage.

Under **Callback URL**, set the Configured URL to be _https://us-central1-PROJECT_ID.cloudfunctions.net/Recording-archiveOnUploaded_ for the Firebase function to receive the archive upload notification. The status will be recorded under the `/data/recording/${meetingId}` along with the S3 URL for the recording file.

More information about the Vonage Video API integration with Bicara can be found under [Bicara Core Step 2](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/3_QuickStart_Vonage.md).

## Local Development

Once the `.runtimeconfig.json` file is prepared under the `/functions` folder (configuration details can be found under [Bicara Core Step 6](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/7_QuickStart_Functions.md)), Firebase Functions can be tested locally using the command:

```
npm run emulators
```

This ensures that the emulator data will be imported when Firebase Emulator starts and exported when the session ends.
